import React, { Component } from 'react';
import Routes from './Routes';
import logo from '../src/img/logoRipley.svg';

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h3>Catálogo de productos</h3>
      </div>
      <div className="App-intro">
        <Routes />
      </div>
    </div>
  );
}
export default App;
