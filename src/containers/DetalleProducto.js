import React, { Component } from 'react';

class DetalleProducto extends Component {    
    render(){
        const product = this.props.location.state.show.product;
        return (            
                <div className="panel-detalle-producto ">
                    <div className="panel-left">
                        <img className="imagen-producto" src={product.fullImage} alt=""/>
                    </div>
                    <div className="panel-right">
                        <p><h1>{product.attributes[0].value}</h1></p>                    
                        <p><h3>{product.name}</h3></p>                    
                        <p>SKU: {product.partNumber}</p>                    
                        <p>{product.shortDescription}</p>
                        <p>Normal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {product.prices.formattedListPrice}</p>
                        <p>Internet &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{product.prices.formattedOfferPrice}</p>
                        <p><span className="card-price">Tarjeta Ripley&nbsp;&nbsp;
                            <img className="catalog-prices__card-price-image" src="//static.ripley.cl/images/opex.png" alt="Precio Tarjeta Ripley"></img>
                            &nbsp;{product.prices.formattedCardPrice}</span></p>                                           
                    </div>
                </div>  
        );
    }
}
export default DetalleProducto;