import React from 'react';
import { Link } from 'react-router-dom';

class ItemProducto extends React.Component {    
    constructor(){
        super()        
    }
    
    
    render(){
        const product= this.props.producto;
        return (
            <div className="panel-item-producto panel panel-default">
                
                <div className="panel-body" >
                    <Link to={{ pathname:'/detalle', state: {show :{product} }}} >           
                        <img className="imagen-producto"  src={this.props.producto.fullImage} alt=""/>  
                    </Link>
                    <div>
                        <h2>{this.props.producto.attributes[0].value}</h2>
                        <span className='subtitulo-producto'>{this.props.producto.name}</span>
                    </div>
                    <div className="catalog-prices-list">
                        <label className="precio-normal" htmlFor="">$&nbsp;<span>{this.props.producto.prices.listPrice}&nbsp;</span></label><br></br>
                        <label className="precio-internet" htmlFor="">$&nbsp;<span>{this.props.producto.prices.offerPrice}&nbsp;</span></label> <br></br>
                        <label className="precio-tarjeta-ripley" htmlFor="">$&nbsp;<span>{this.props.producto.prices.cardPrice}&nbsp;</span></label>
                        <img className="catalog-prices__card-price-image" src="//static.ripley.cl/images/opex.png" alt="Precio Tarjeta Ripley"></img>
                    </div>                     
                </div>      
            </div>
        );
    }      
}
export default ItemProducto;