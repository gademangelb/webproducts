import React, { Component } from 'react';
import '../App.css'
import request from 'superagent';
import ItemProducto from '../components/ItemProducto';


class Home extends Component {  
    constructor(){
        super();          
        this.state = {
           productos: ''
        }        
        this.getProducts();         
      }
    
    getToken(){ 
        
            request
            .get('https://afternoon-journey-77837.herokuapp.com/api/products/login')
            .set('Content-Type','application/json')
            .set('x-credentials','ewogICAgInVzZXJuYW1lIjoidXN1YXJpb0Zyb250RGVzYWZpbyIsCiAgICAicGFzcyI6InVzdWFyaW9Gcm9udERlc2FmaW8xMjMiCn0=')
            .end((req, res)  => {                          
               var tokenNuevo = res.body.message;  
               sessionStorage.setItem('user-session',tokenNuevo);
            });        
    }
    
    getProducts(){       
        const userSession = sessionStorage.getItem('user-session');
        try {
            request
            .get('https://afternoon-journey-77837.herokuapp.com/api/products/some')
            .set('Content-Type','application/json')
            .set('Authorization',`Bearer ${userSession}`)
            .end((req, res)  => {
                if (res.badRequest) {
                    console.info('Error simulado');
                    this.getProducts();
                }  
                if (res.unauthorized) {
                    this.getToken();
                    this.getProducts();
                }                 
                var productosPagina = JSON.parse(res.body.message); 
                this.setState({
                    productos: productosPagina                    
                });
                this.props = productosPagina;
            })
        
        } catch (error) {
            throw error.message;
        }
        
    }     
    
    render(){
        var indents = [];
        var productos = this.state.productos;
        for (var i = 0; i < productos.length; i++) {
            var producto = productos[i];
            indents.push(
                <div className="item-producto" key={i}>
                    <ItemProducto producto = {producto}/>
                </div>
                );
        }

        return (
            <div className="imagen-fondo-principal container-catalogo-productos">
                <div className="panel-catalogo-productos panel panel-default">
                    <div className="panel-heading">
                    <span className="panel-title"></span>                    
                    </div>
                    <div className="panel-body">                    
                        {indents}                
                    </div>
                </div>
            </div>
        );
    }    
}
export default Home; 