import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './components/Home'
import Detalle from './containers/DetalleProducto';

const Routes = () => {

    return(
        <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/detalle' component ={Detalle} />
        </Switch>
    )
}

export default Routes;